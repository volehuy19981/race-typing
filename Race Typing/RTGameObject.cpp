#include "RTGameObject.hpp"
#include "RTTextureManager.hpp"
#include "Variables.hpp"

RTGameObject::RTGameObject(SDL_Renderer* renderer, std::string content, int posX, int posY, std::string font, int fontSize) :
	_content(content),
	_size(fontSize),
	_spVelocityX(0),
	_velocityX(VELOCITY_X_GAME_OBJECT),
	_fontName(font),

	// sdl struct
	_renderer(renderer),
	_texture(nullptr),
	_font(nullptr)
{
	_texture = RTTextureManager::LoadTextureFromText(_renderer, content, _fontName, _size, &_wrapper);
	_wrapper.x = posX;
	_wrapper.y = posY;
}

const SDL_Rect& RTGameObject::GetObjectBody()
{
	return _wrapper;
}

const int& RTGameObject::GetPositionX()
{
	return _wrapper.x;
}

const std::string& RTGameObject::GetContent()
{
	return _content;
}

void RTGameObject::SetPositionX(int x)
{
	_wrapper.x = x;
}

void RTGameObject::SetPositionY(int y)
{
	_wrapper.y = y;
}

void RTGameObject::SetContentColor(int length, int r, int g, int b)
{
	std::string matchStr = _content.substr(0, length);
	_matchTexture = RTTextureManager::LoadTextureFromText(_renderer, matchStr, _fontName, _size, &_matchWrapper);
	_matchWrapper.x = _wrapper.x;
	_matchWrapper.y = _wrapper.y;
	SDL_SetTextureColorMod(_matchTexture, r, g, b);
}

void RTGameObject::SetContentColor(int r, int g, int b)
{
	SDL_SetTextureColorMod(_texture, r, g, b);
}

void RTGameObject::DestroyMatchTexture()
{
	if (_matchTexture != nullptr)
	{
		SDL_DestroyTexture(_matchTexture);
	}
}

void RTGameObject::Update()
{
	_spVelocityX++;
	
	if (_spVelocityX % UNNAMED_FOR_VELOCITY == 0)
	{
		// increase velocity x
		_wrapper.x += _velocityX;
		_matchWrapper.x = _wrapper.x;
	}
}

void RTGameObject::Render()
{
	SDL_RenderCopy(_renderer, _texture, NULL, &_wrapper);
	if (_matchTexture != nullptr)
	{
		SDL_RenderCopy(_renderer, _matchTexture, NULL, &_matchWrapper);
	}
}

void RTGameObject::Clean()
{
	// destroy texture
	SDL_DestroyTexture(_texture);
	DestroyMatchTexture();
}

RTGameObject::~RTGameObject()
{
}