#pragma once

#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

class RTGameObject
{
private:
	int _size;
	int _spVelocityX;
	int _velocityX;
	std::string _content;
	std::string _fontName;

	// sdl struct
	SDL_Renderer* _renderer;
	SDL_Texture* _texture;
	SDL_Texture* _matchTexture;
	SDL_Color _contentColor;
	SDL_Rect _wrapper;
	SDL_Rect _matchWrapper;
	TTF_Font* _font;
public:
	RTGameObject(SDL_Renderer* renderer, std::string content, int posX, int posY, std::string font, int size);

#pragma region GET
	const int& GetPositionX();
	const std::string& GetContent();
	const SDL_Rect& GetObjectBody();
#pragma endregion

#pragma region SET
	void SetPositionX(int x);
	void SetPositionY(int y);
	void SetContentColor(int length, int r, int g, int b);
	void SetContentColor(int r, int g, int b);
#pragma endregion

	void DestroyMatchTexture();
	void Update();
	void Render();
	void Clean();

	~RTGameObject();
};