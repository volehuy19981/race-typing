#pragma once

#include <string>

constexpr int VELOCITY_X_GAME_OBJECT = 20;
constexpr int UNNAMED_FOR_VELOCITY = 20;
constexpr float WARNING_ZONE_PERCENT = 0.5;
constexpr float DANGEROUS_ZONE_PERCENT = 0.7;
constexpr SDL_Color WHITE = { 0xFF, 0xFF, 0xFF };
constexpr int FONT_SIZE = 13;
constexpr int FPS = 60;
constexpr int MAXIMUM_CHARACTER = 30;
const std::string FONT_NAME = "arialbd.ttf";