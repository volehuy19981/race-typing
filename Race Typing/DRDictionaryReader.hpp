#pragma once

#include <vector>
#include <string>

class __declspec(dllexport) DRDictionaryReader
{
public:
	static std::vector<std::string> Read(const char* filePath);
};