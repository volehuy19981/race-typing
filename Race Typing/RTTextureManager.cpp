#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "RTTextureManager.hpp"
#include "Variables.hpp"

SDL_Texture* RTTextureManager::LoadTextureFromText(SDL_Renderer* renderer, std::string content, std::string font, int fontSize, SDL_Rect* originSize)
{
	SDL_Texture* result = nullptr;
	TTF_Font* ttfFont = nullptr;
	SDL_Surface* contentSrf = nullptr;

	do
	{
		// create sdl font
		if (!(ttfFont = TTF_OpenFont(font.c_str(), fontSize)))
		{
			// create font fail
			SDL_Log("Create font failed: %s", SDL_GetError());
			break;
		}

		// create surface
		if (!(contentSrf = TTF_RenderText_Solid(ttfFont, content.c_str(), WHITE)))
		{
			// create surface fail
			SDL_Log("Create surface failed");
			break;
		}

		// create texture
		if (!(result = SDL_CreateTextureFromSurface(renderer, contentSrf)))
		{
			// create texture fail
			SDL_Log("Create texture failed");
			break;
		}

		if (originSize != nullptr)
		{
			originSize->w = contentSrf->w;
			originSize->h = contentSrf->h;
		}

		// destroy surface
		SDL_FreeSurface(contentSrf);
	} while (false);

	return result;
}
