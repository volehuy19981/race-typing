#pragma once

#include <string>

class _declspec(dllexport) CFConfigurationFile
{
private:
	int _screenWidth;
	int _screenHeight;
	bool _isFullScreen;
	std::string _configFilePath;
	std::string _gameTitle;
public:
	CFConfigurationFile();
#pragma region GET
	int GetScreenWidth();
	int GetScreenHeight();
	std::string GetGameTitle();
	std::string GetConfigFilePath();
	bool IsFullScreen();
#pragma endregion
#pragma region SET
	void SetGameTittle(std::string gameTitle);
	void SetScreenWidth(int screenWidth);
	void SetScreenHeight(int screenHeight);
	void SetFullScreen(bool fullScreen);
#pragma endregion
};