#include <cctype>
#include <algorithm>
#include <random>
#include <chrono>
#include <SDL2/SDL.h>
#include <SDL2/SDL_thread.h>
#include "RTGame.hpp"
#include "RTGameObject.hpp"
#include "RMRandomMachine.hpp"
#include "DRDictionaryReader.hpp"
#include "RTTextureManager.hpp"
#include "Variables.hpp"

bool IsWarningZone(int x, int widthRange)
{
	return x > widthRange * WARNING_ZONE_PERCENT;
}

bool IsDangerZone(int x, int widthRange)
{
	return x > widthRange * DANGEROUS_ZONE_PERCENT;
}

RTGame::RTGame() :
	_x(0),
	_y(0),
	_widthScreen(0),
	_heightScreen(0),
	_score(0),
	_isFullScreen(false),
	_isRunning(false),
	_title("Unknown"),
	_userInput(""),
	
	// SDL struct
	_window(nullptr),
	_renderer(nullptr),
	_correctSound(nullptr),
	_wrongSound(nullptr),
	_timeObj(nullptr),
	_userInputObj(nullptr),
	_goodScoreObj(nullptr)
{
}

void RTGame::Init(const char* title, int posX, int posY, int widthScreen, int heightScreen, bool isFullScreen)
{
	_title = title;
	_x = posX;
	_y = posY;
	_widthScreen = widthScreen;
	_heightScreen = heightScreen;
	_isFullScreen = isFullScreen;
	std::vector<std::string> contents;

	do
	{
		if (SDL_Init(SDL_INIT_EVERYTHING))
		{
			// init sdl fail
			SDL_Log("Init SDL2 fail: %s", SDL_GetError());
			break;
		}

		SDL_initFramerate(&_fpsManager);

		// init 60ps
		if (SDL_setFramerate(&_fpsManager, FPS) != 0)
		{
			// set fps fail
			SDL_Log("Init fps fail: %s", SDL_GetError());
			break;
		}

		_window = SDL_CreateWindow(_title, _x, _y, _widthScreen, _heightScreen, _isFullScreen);
		if (_window == nullptr)
		{
			// create sdl window fail
			SDL_Log("Init SDL2 Window fail: %s", SDL_GetError());
			break;
		}

		_renderer = SDL_CreateRenderer(_window, -1, 0);
		if (_renderer == nullptr)
		{
			// create sdl renderer fail
			SDL_Log("Init SDL2 Renderer fail: %s", SDL_GetError());
			break;
		}

		if (TTF_Init() != 0)
		{
			// create ttf fail
			SDL_Log("Init TTF2 fail: %s", SDL_GetError());
			break;
		}

		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
		{
			// create sound fail
			SDL_Log("Init sound fail: %s\n", Mix_GetError());
			break;
		}

		_correctSound = Mix_LoadWAV("correct_effect.wav");
		if (_correctSound == nullptr)
		{
			// create correct sound fail
			SDL_Log("Init correct sound fail: %s\n", Mix_GetError());
		}

		_wrongSound = Mix_LoadWAV("wrong_effect.wav");
		if (_wrongSound == nullptr)
		{
			// create wrong sound fail
			SDL_Log("Init wrong sound fail: %s\n", Mix_GetError());
		}

		_retrySound = Mix_LoadWAV("retry_effect.wav");
		if (_retrySound == nullptr)
		{
			// create retry sound fail
			SDL_Log("Init retry sound fail: %s\n", Mix_GetError());
		}

		contents = DRDictionaryReader::Read("dict_cpp.txt");
		if (contents.empty())
		{
			// load dictionary fail
			SDL_Log("Load dictionary cpp fail: %s", SDL_GetError());
			break;
		}

		// suffle dictionaries
		std::random_device rd;
		std::shuffle(std::begin(contents), std::end(contents), std::default_random_engine{ rd() });

		for (auto& content : contents)
		{
			// random position
			int x = RMRandomMachine::GetInteter(-5000, 0);
			int y = RMRandomMachine::GetInteter(0, _heightScreen);
			RTGameObject* gameObj = new RTGameObject(_renderer, content, x, y, FONT_NAME, FONT_SIZE);

			// check disapear object 
			if (y + gameObj->GetObjectBody().h >= _heightScreen)
			{
				// out of screen height, reset y position
				gameObj->SetPositionY(_heightScreen - gameObj->GetObjectBody().h);
			}

			// add to list
			_textObjs.emplace_back(gameObj);
		}

		_goodScoreObj = new RTGameObject(_renderer, std::to_string(_score), 0, 0, FONT_NAME, FONT_SIZE);
		if (_goodScoreObj == nullptr)
		{
			// create correct score fail
			SDL_Log("Create correct score fail: %s", SDL_GetError());
			break;
		}

		SDL_StartTextInput();

		// don't throw exception or break immediately so we will think success
		_isRunning = true;

	} while (false);
}

bool RTGame::IsRunning()
{
	return _isRunning;
}

void RTGame::HandleEvents()
{
	int prevScore = _score;
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT: _isRunning = false; break;
		case SDL_TEXTINPUT:
			_userInput += event.text.text;
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym)
			{
			case SDLK_BACKSPACE:
				_userInput = _userInput.size() > 0 ? _userInput.substr(0, _userInput.size() - 1) : _userInput;
				break;
			case SDLK_RETURN:
				for (int i = 0; i < _textObjs.size(); i++)
				{
					if (0 < _textObjs[i]->GetPositionX() && _textObjs[i]->GetPositionX() < _widthScreen)
					{
						// in user viewable
						if (_userInput == _textObjs[i]->GetContent())
						{
							Mix_PlayChannel(-1, _correctSound, 0);
							// match string
							_userInput = "";

							// increase score
							_score++;
							SDL_Log("%d", _score);

							// delete word
							_textObjs[i]->Clean();
							delete _textObjs[i];
							_textObjs[i] = nullptr;
							_textObjs.erase(_textObjs.begin() + i);
						}
					}
				}

				if (prevScore == _score)
				{
					Mix_PlayChannel(-1, _wrongSound, 0);
				}

				break;
			case SDLK_ESCAPE:
				Mix_PlayChannel(-1, _retrySound, 0);
				_userInput = "";
				for (auto& textObj : _textObjs)
				{
					textObj->DestroyMatchTexture();
				}
				break;
			default: break;
			}
#ifndef DEBUG
			SDL_Log("%s", _userInput.c_str());
#endif
			break;
		default: break;
		}
	}
}

void RTGame::Update()
{
#pragma region TIME VIEW
	if (_timeObj != nullptr)
	{
		_timeObj->Clean();
		delete _timeObj;
		_timeObj = nullptr;
	}
	char timeLine[26] = "\0";
	auto time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	ctime_s(timeLine, sizeof(timeLine), &time);
	if (timeLine[strlen(timeLine) - 1] == '\n')
	{
		timeLine[strlen(timeLine) - 1] = '\0';
	}
	_timeObj = new RTGameObject(_renderer, timeLine, 0, 0, FONT_NAME, FONT_SIZE);
	_timeObj->SetPositionX(_widthScreen - _timeObj->GetObjectBody().w);
#pragma endregion

#pragma region USER INPUT VIEW
	if (_userInputObj != nullptr)
	{
		_userInputObj->Clean();
		delete _userInputObj;
		_userInputObj = nullptr;
	}

	int remainCharacter = MAXIMUM_CHARACTER - _userInput.size();
	std::string remainCharacters;
	remainCharacters += "[";
	if (remainCharacter / 10 == 0 && remainCharacter >= 0)
	{
		remainCharacters += "0";
	}

	remainCharacters += std::to_string(remainCharacter);
	remainCharacters += "]";

	std::string temp = _userInput;
	if (_userInput.size() > MAXIMUM_CHARACTER)
	{
		temp = _userInput.substr(_userInput.size() - MAXIMUM_CHARACTER, MAXIMUM_CHARACTER);
	}

	_userInputObj = new RTGameObject(_renderer, remainCharacters + temp, 0, 0, FONT_NAME, FONT_SIZE);
	_userInputObj->SetPositionY(_heightScreen - _userInputObj->GetObjectBody().h);
#pragma endregion	

#pragma region TEXT VIEW
	for (int idx = 0; idx < _textObjs.size(); idx++)
	{
		if (IsDangerZone(_textObjs[idx]->GetPositionX(), _widthScreen))
		{
			// dangerous zone, red color
			_textObjs[idx]->SetContentColor(0xff, 0x0, 0x0);
		}
		else if (IsWarningZone(_textObjs[idx]->GetPositionX(), _widthScreen))
		{
			// warning zone, yellow color
			_textObjs[idx]->SetContentColor(0xff, 0xff, 0x0);
		}
	}

	// find match
	for (int idx = 0; idx < _textObjs.size(); idx++)
	{
		if (_textObjs[idx]->GetPositionX() > 0)
		{
			// maybe in view screen
			if (_textObjs[idx]->GetPositionX() < _widthScreen)
			{
				// in viewable, find substring start with user input
				if (!_userInput.empty())
				{
					std::size_t found = _textObjs[idx]->GetContent().rfind(_userInput, 0);
					if (found != std::string::npos)
					{
						// match substring
						_textObjs[idx]->SetContentColor(_userInput.size(), 0x0, 0xff, 0x0);
					}
					else
					{
						// not found start with substring user input, destroy match texture
						_textObjs[idx]->DestroyMatchTexture();
					}
				}
				else
				{
					_textObjs[idx]->DestroyMatchTexture();
				}
			}
			else
			{
				// out of screen
				_textObjs.erase(_textObjs.begin() + idx);
			}
		}
	}

	for (auto& ptrText : _textObjs)
	{
		ptrText->Update();
	}
#pragma endregion

	_goodScoreObj->Clean();
	delete _goodScoreObj;
	_goodScoreObj = nullptr;
	_goodScoreObj = new RTGameObject(_renderer, "Correct: " + std::to_string(_score), 0, 0, FONT_NAME, FONT_SIZE);
	_goodScoreObj->SetPositionX(_widthScreen - _goodScoreObj->GetObjectBody().w);
	_goodScoreObj->SetPositionY(_heightScreen - _goodScoreObj->GetObjectBody().h);
}

void RTGame::SetColorBackground()
{
	// set default color is night
	SDL_SetRenderDrawColor(_renderer, 0x00, 0x0, 0x0, 0x0);
	SDL_RenderClear(_renderer);
}

void RTGame::RenderScreen()
{
	SetColorBackground();

#pragma region TIME RENDER
	_timeObj->SetContentColor(_timeObj->GetContent().size(), 0xff, 0xff, 0xff);
	_timeObj->Render();
#pragma endregion

#pragma region USER INPUT RENDER
	if (_userInputObj != nullptr)
	{
		_userInputObj->SetContentColor(_userInputObj->GetContent().size(), 0xff, 0xff, 0xff);
		_userInputObj->Render();
	}
#pragma endregion

#pragma region TEXT RENDER
	for (auto& ptrText : _textObjs)
	{
		ptrText->Render();
	}
#pragma endregion

	_goodScoreObj->SetContentColor(0x0, 0xff, 0x0);
	_goodScoreObj->Render();

	// render to window
	SDL_RenderPresent(_renderer);
}

void RTGame::Clean()
{
#pragma region TEXT CLEANER
	for (auto& ptrText : _textObjs)
	{
		ptrText->Clean();
		delete ptrText;
		ptrText = nullptr;
	}

	SDL_StopTextInput();
#pragma endregion
	
#pragma region TIME CLEANER
	if (_timeObj != nullptr)
	{
		_timeObj->Clean();
		delete _timeObj;
		_timeObj = nullptr;
	}
#pragma endregion

#pragma region USER INPUT CLEANER
	if (_userInputObj != nullptr)
	{
		_userInputObj->Clean();
		delete _userInputObj;
		_userInputObj = nullptr;
	}
#pragma endregion

	Mix_FreeChunk(_correctSound);
	Mix_FreeChunk(_wrongSound);
	Mix_FreeChunk(_retrySound);

	if (_goodScoreObj != nullptr)
	{
		_goodScoreObj->Clean();
		delete _goodScoreObj;
		_goodScoreObj = nullptr;
	}

	// destroy renderer
	SDL_DestroyRenderer(_renderer);

	// destroy window
	SDL_DestroyWindow(_window);

	Mix_Quit();
	SDL_Quit();
}

void RTGame::Delay()
{
	SDL_framerateDelay(&_fpsManager);
}

RTGame::~RTGame()
{
}
