#pragma once

#include <string>

struct SDL_Texture;
struct SDL_Renderer;
struct SDL_Rect;

class RTTextureManager
{
public:
	static SDL_Texture* LoadTextureFromText(SDL_Renderer* renderer, std::string content, std::string font, int size, SDL_Rect* originSize = nullptr);
};