#include "RTGame.hpp"
#include "CRConfigurationReader.hpp"

int main(int argc, char** argv)
{
	RTGame* game = nullptr;
	CRConfigurationReader configReader;
	CFConfigurationFile configFile; 

	configReader.Read();
	configFile = configReader.GetConfig();

	game = new RTGame();
	game->Init(configFile.GetGameTitle().c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		configFile.GetScreenWidth(),
		configFile.GetScreenHeight(),
		configFile.IsFullScreen());

	while (game->IsRunning())
	{
		game->HandleEvents();
		game->Update();
		game->RenderScreen();
		game->Delay();
	}

	game->Clean();
	delete game;

	return 0;
}