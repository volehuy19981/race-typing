#pragma once

#include <string>
#include <vector>
#include <SDL2/SDL2_framerate.h>
#include <SDL2/SDL_mixer.h>

struct SDL_Window;
struct SDL_Renderer;

struct RTGameObject;

class RTGame
{
private:
	int _x;
	int _y;
	int _widthScreen;
	int _heightScreen;
	int _score;
	bool _isFullScreen;
	bool _isRunning;
	const char* _title;
	std::string _userInput;

	// SDL2 struct
	SDL_Window* _window;
	SDL_Renderer* _renderer;
	FPSmanager _fpsManager;
	Mix_Chunk* _correctSound;
	Mix_Chunk* _wrongSound;
	Mix_Chunk* _retrySound;

	RTGameObject* _timeObj;
	RTGameObject* _userInputObj;
	RTGameObject* _goodScoreObj;
	std::vector<RTGameObject*> _textObjs;
public:
	RTGame();
	void Init(const char* title, int posX, int posY, int widthScreen, int heightScreen, bool isFullScreen = false);
	bool IsRunning();
	void HandleEvents();
	void Update();
	void SetColorBackground();
	void RenderScreen();
	void Clean();
	void Delay();
	~RTGame();
};
