#pragma once

#include "CFConfigurationFile.hpp"

class _declspec(dllexport) CRConfigurationReader
{
private:
	CFConfigurationFile _configFile;
public:
	void Read();
	CFConfigurationFile GetConfig();
};